import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  count_right: number = 0;
  count_wrong: number = 0;
  a: number = 0;
  b: number = 0;
  answer: string = '';
  right: boolean = undefined;

  c: number = 0;

  constructor() { }

  ngOnInit() {
    this.add_problem();
  }

  add_problem() {
    let max = 100;
    this.c = Math.floor(Math.random() * (max - 1)) + 1;
    this.a = Math.floor(Math.random() * (this.c - 2)) + 1;
    this.b = this.c  - this.a;
    this.answer = '';
    this.right = undefined;
  }

  may_check() {
    return this.answer !== '' && this.right === undefined;
  }

  check_answer() {
    console.log('check_answere()')
    this.right = this.c === parseInt(this.answer);
    this.count_right += this.right ? 1 : 0;
    this.count_wrong += this.right ? 0 : 1;
  }

  new_problem() {
    console.log('new_problem()')
    this.add_problem();
  }

}
